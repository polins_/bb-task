function myFunction() {
    let x = document.getElementById("mobileNav");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }

//----------

let slideIndex = 0;
showSlides();

function showSlides() {
    let i;
    let slides = document.getElementsByClassName("slideshow__slide");

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
    slideIndex++;
  
    if (slideIndex > slides.length) {
        slideIndex = 1
    }    

    slides[slideIndex-1].style.display = "block";  

    setTimeout(showSlides, 5000); 
}

//----------

function activateAccordion(){
  let acc = document.getElementsByClassName("expander__toggle");
   
  for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {

      let newWidth = document.documentElement.clientWidth; //window.innerWidth;  

      if(newWidth <= 1024){
        this.classList.toggle("active");
        let panel = this.nextElementSibling;
        
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      } 

    });
  }
}

window.addEventListener('load', function(event){
  activateAccordion();
});
  



